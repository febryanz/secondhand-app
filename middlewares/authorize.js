require("dotenv").config();
const jwt = require("jsonwebtoken");
// models
const { Token } = require("../models");

module.exports = async (req, res, next) => {
  try {
    // request header
    const [Bearer, token] = req.get("Authorization").split(' ');
    
    // verify token
    if (Bearer == "bearer") {
      return res.status(401).json({
        success: false,
        message: "Invalid token"
      });
    } else {
      const checkToken = await Token.findOne({
        where: { token }
      });
      if (checkToken) {
        jwt.verify(token, process.env.JWT_SECRET_KEY, { issuer: process.env.JWT_ISSUER },
          async (err, isVerify) => {
            if (err) {
              await Token.destroy({
                where: {token}
              });
              return res.status(401).json({
                success: false,
                message: err.message
              });
            }

            if (isVerify) {
              req.user = isVerify;
              next()
            } else {
              return res.status(401).json({
                success: false,
                message: "Invalid token"
              });
            }
          });
      } else {
        return res.status(401).json({
          success: false,
          message: "Invalid token"
        });
      }
    }
  } catch (error) {
    next(error);
  }
}
class BaseError extends Error {
  constructor(message, statusCode, details) {
    super(message);

    this.message = message;
    this.statusCode = statusCode;
    this.details = details;
    Error.captureStackTrace(this);
  }
}

module.exports = BaseError;
const BaseError   = require("./BaseError");
const statusCodes = require("./statusCodes");

class Api400Error extends BaseError {
  constructor(message, details = [], statusCode = statusCodes.BAD_REQUEST) {
    super(message, statusCode, details);
  }
}

class Api401Error extends BaseError {
  constructor(message, details = [], statusCode = statusCodes.UNAUTHORIZED) {
    super(message, statusCode, details);
  }
}

class Api403Error extends BaseError {
  constructor(message, details = [], statusCode = statusCodes.FORBIDDEN) {
    super(message, statusCode, details);
  }
}

class Api404Error extends BaseError {
  constructor(message, details = [], statusCode = statusCodes.NOT_FOUND) {
    super(message, statusCode, details);
  }
}

class Api422Error extends BaseError {
  constructor(message, details = [], statusCode = statusCodes.UNPROCESSABLE_ENTITY) {
    super(message, statusCode, details);
  }
}

class Api500Error extends BaseError {
  constructor(message, details = [], statusCode = statusCodes.INTERNAL_SERVER) {
    super(message, statusCode, details);
  }
}

module.exports = {
  Api400Error, Api401Error, Api403Error, Api404Error, Api422Error, Api500Error
}
require("dotenv").config();
const jwt = require("jsonwebtoken");

// models
const { Token } = require("../models");

module.exports = async (req, res, next) => {
  try {
    const authorize = req.get("Authorization");

    if (authorize == undefined || authorize == "") {
      // sign auth in request
      req.auth = false;
      next()
    } else {
      const token = authorize.split(" ")[1];
      const checkToken = await Token.findOne({
        where: { token }
      });
      if (checkToken) {
        jwt.verify(token, process.env.JWT_SECRET_KEY, { issuer: process.env.JWT_ISSUER },
          async (err, isVerify) => {
            if (err) {
              await Token.destroy({
                where: {token}
              });
              return res.status(401).json({
                success: false,
                message: err.message
              });
            }

            if (isVerify) {
              req.auth = true;
              req.user = isVerify;
              next()
            } else {
              return res.status(401).json({
                success: false,
                message: "Invalid token"
              });
            }
          });
      } else {
        return res.status(401).json({
          success: false,
          message: "Invalid token"
        });
      }
    }
  } catch (error) {
    next(error);
  }
}
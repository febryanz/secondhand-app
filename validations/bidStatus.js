const { param } = require("express-validator");

module.exports = [
  param("status")
    .notEmpty().withMessage("status cannot be empty")
    .isIn(["accept", "reject", "canceled", "completed"]).withMessage("status must be contain accept, reject, canceled or completed")
]
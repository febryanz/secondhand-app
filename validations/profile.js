const { body } = require("express-validator");

module.exports = [
  body("name").isString().optional(),
  body("city").isString().optional(),
  body("phone_number").isString().optional(),
  body("address").isString().optional(),
]
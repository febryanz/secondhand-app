const { body } = require("express-validator");

module.exports = [
  body("name")
    .notEmpty().withMessage("name is not empty")
    .isString(),
  body('email')
    .isEmail().withMessage("email format does not match")
    .notEmpty().withMessage("email is not empty"),
  body("password")
    .notEmpty().withMessage("password is not empty")
    .isLength({ min: 8, max: 20 }).withMessage("minimum password length is 8 and maximum 20 characters")
    .isString()
]
const { body } = require("express-validator");

module.exports = [
  body("price_bid")
    .notEmpty().withMessage("price_bid cannot be empty")
    .isInt().withMessage("price_bid must be number")
]
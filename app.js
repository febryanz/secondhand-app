require("dotenv").config();
const express         = require('express');
const path            = require('path');
const logger          = require('morgan');
const cors            = require('cors');
const compression     = require('compression');
const swaggerUi       = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const app = express();

app.use(cors({
  origin: "*"
}))
app.use(compression({ level: 1 }));
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/v1/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

// routes
app.use(require("./routes"));

// catch error
app.use(require('./errorHandler'));

module.exports = app;

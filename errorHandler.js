module.exports = (err, req, res, next) => {
  if (err.statusCode) {
    return res.status(err.statusCode).json({
      success: false,
      message: err.message,
      details: err.details
    });
  } else {
    return res.status(500).json({
      success: false,
      message: err.message,
      details: err.details
    });
  }
}
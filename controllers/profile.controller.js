const cloudinary = require("cloudinary").v2;
const multer     = require("multer");
const path       = require("path");
const fs         = require("fs");
const upload     = require("../helper/multer.helper")().single("image");

// config cloudinary
cloudinary.config({
  cloud_name: process.env.CLD_CLOUD_NAME,
  api_key: process.env.CLD_API_KEY,
  api_secret: process.env.CLD_API_SECRET,
  secure: true
});

// API Error
const { validationResult } = require("express-validator");
const { Api404Error, Api400Error, Api422Error } = require("../middlewares/errors/APIErrors");

// models
const { Users } = require("../models");

class ProfileController {
  static async getDetails(req, res, next) {
    try {
      // querying data user
      const user = await Users.findOne({
        where: { id: req.user.id },
        attributes: ["id", "email", "name", "city", "address", "phone_number", "avatar"]
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      if (user) {
        return res.json({
          success: true,
          message: "data successfully retrieved",
          user
        });
      } else {
        throw new Api404Error("profile not found");
      }
    } catch (error) {
      next(error);
    }
  }

  static async updateProfile(req, res, next) {
    upload(req, res, async function (err) {
      try {
        if (err instanceof multer.MulterError) {
          throw new Api400Error(err);
        } else if (err) {
          throw new Api400Error(err);
        }

        // check request body validation
        const errors = validationResult(req);
        if (!errors.isEmpty()) {
          throw new Api422Error("validation error", errors.array());
        }

        if (req.file != null) {
          // query profile
          const user = await Users.findOne({
            where: { id: req.user.id },
            attributes: ["avatar"]
          });
          // destroy previous image
          if (user.avatar != null) {
            const url = user.avatar;
            let public_id = url.split('/').slice(7, 9);
            public_id[1] = public_id[1].split('.')[0];
            public_id = public_id.join('/');
            // destroy avatar
            console.log(public_id);
            await cloudinary.uploader.destroy(public_id);
          }
          // upload file
          await cloudinary.uploader.upload(req.file.path, { folder: "profile" }, async (err, done) => {
            if (err) {
              throw new Api400Error(err.message);
            }
            // remove file from public directory
            fs.unlinkSync(path.join(__dirname,'../' + req.file.path));
            // updating data
            req.body.avatar = done.secure_url;
            await Users.update(req.body, {
              where: { id: req.user.id }
            })
            .catch(error => {
              throw new Api400Error(error.message);
            });

            return res.json({
              success: true,
              message: "user successfully updated"
            });
          });
        } else {
          await Users.update(req.body, {
            where: { id: req.user.id }
          })
          .catch(error => {
            throw new Api400Error(error.message);
          });

          return res.json({
            success: true,
            message: "user successfully updated"
          });
        }
      } catch (error) {
        // delete image
        fs.unlinkSync(path.join(__dirname,'../' + req.file.path));
        next(error);
      }
    })
  }
}

module.exports = ProfileController;
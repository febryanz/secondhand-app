// models
const { Notification } = require("../models");

// API Error 
const { Api404Error, Api400Error } = require("../middlewares/errors/APIErrors");

class NotificationController {
  static async changeNotificationStatus(req, res, next) {
    try {
      // get id notification
      const { id } = req.params;

      // change status notification
      await Notification.update({ isRead: "Y" }, {
        where: { id }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      return res.json({
        success: true,
        message: "Notification status successfully changed"
      });
    } catch (error) {
      next(error);
    }
  }

  static async getAllNotification(req, res, next) {
    try {
      const { page = 1, limit = 10 } = req.query;
      
      // querying data notification
      const notification = await Notification.findAll({
        where: {
          user_id: req.user.id,
          isRead: "N"
        },
        order: [["updatedAt", "DESC"]],
        limit,
        offset: +limit * +page - +limit
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      if (notification) {
        return res.json({
          success: true,
          message: "data successfully retrieved",
          notification
        });
      } else {
        throw new Api404Error("notification not found");
      }
    } catch (error) {
      next(error);
    }
  }
}

module.exports = NotificationController;
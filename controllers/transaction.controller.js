// models
const { Users, Bids, Products, Notification, productImages } = require("../models");
const { Op } = require("sequelize");

// API Error
const { validationResult } = require("express-validator");
const { Api422Error, Api400Error, Api404Error } = require("../middlewares/errors/APIErrors");

class TransactionController {
  static async BidsProduct(req, res, next) {
    try {
      // get id product from params
      const { id } = req.params;
      
      // catch error
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw new Api422Error("validation error", errors.array());
      }

      // check profile buyer
      const profile = await Users.findOne({
        where: { id: req.user.id },
        attributes: ["name", "city", "address", "phone_number", "avatar"]
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      for (const d in profile) {
        if (profile[d] == null) {
          throw new Api400Error("Your profile is not complete, please complete your profile")
        }
      }

      // check availability bid
      const checkAVB = await Bids.findOne({
        where: {
          buyer_id: req.user.id,
          product_id: id
        }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      if (checkAVB !== null) {
        throw new Api400Error("the product has been offered");
      }

      // check product
      const checkPRD = await Products.findOne({
        where: { id, status: "unpublished" }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      if (checkPRD) {
        throw new Api400Error("cannot bid this product, product unpublished")
      }

      // add bid
      const productInfo = await Products.findOne({
        where: { id },
        attributes: ["user_id"]
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      await Bids.create({
        product_id: id,
        buyer_id: req.user.id,
        seller_id: productInfo.user_id,
        price_bid: req.body.price_bid
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      // send notification
      await Notification.bulkCreate([
        { user_id: req.user.id, product_id: id, as: "buyer", isRead: "N", message: "Penawaran telah dikirim" },
        { user_id: productInfo.user_id, product_id: id, as: "seller", isRead: "N", message: "Penawaran masuk" }
      ])
      .catch(error => {
        throw new Api400Error(error.message);
      });

      return res.status(201).json({
        success: true,
        message: "bid successfully sent to seller"
      });
    } catch (error) {
      next(error);
    }
  }

  static async getDetailBid(req, res, next) {
    try {
      // get id bid from params
      const { id } = req.params;
      // querying
      const bid = await Bids.findOne({
        where: { id, seller_id: req.user.id },
        include: [
          {
            model: Users,
            as: "buyer",
            attributes: ["id", "email", "name", "city", "address", "phone_number", "avatar"]
          },
          {
            model: Products,
            as: "product",
            attributes: ["name", "price", "category", "status", "description"],
            include: {
              model: productImages,
              as: "images"
            }
          }
        ]
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      // querying notification
      const notification = await Notification.findAll({
        where: {
          user_id: req.user.id,
          isRead: "N"
        }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      if (bid == null) {
        throw new Api404Error("data not found");
      }

      return res.json({
        success: true,
        message: "detail bid successfully retrieved",
        bid,
        notification
      })
    } catch (error) {
      next(error);
    }
  }

  static async getEntryBid(req, res, next) {
    try {
      // querying bid
      const bids = await Bids.findAll({
        where: {
          seller_id: req.user.id,
          status: {
            [Op.not]: "completed"
          }
        },
        include: {
          model: Products,
          as: "product",
          attributes: ["name", "price", "category", "status", "description"],
          include: {
            model: productImages,
            as: "images",
            attributes: ["id", "path"]
          }
        }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });
      // querying notification
      const notification = await Notification.findAll({
        where: {
          user_id: req.user.id,
          isRead: "N"
        }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      return res.json({
        success: true,
        message: "bidding data successfully retrieved",
        bids,
        notification
      });
    } catch (error) {
      next(error);
    }
  }

  static async getTransactionHistories(req, res, next) {
    try {
      // querying
      const history = await Products.findAll({
        where: { user_id: req.user.id, status: "sold" },
        include: {
          model: productImages,
          as: "images"
        }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      return res.json({
        success: true,
        message: "transaction history successfully retrieved",
        history
      })
    } catch (error) {
      next(error);
    }
  }

  static async changeStatusBid(req, res, next) {
    try {
      // get data id and status from params
      const { id, status } = req.params;

      // checking
      const bidInfo = await Bids.findOne({
        where: { id },
        attributes: ["status", "product_id"]
      });

      // check validation
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw new Api422Error("validation error", errors.array());
      }

      if (bidInfo.status == "pending" && (status == "completed" || status == "canceled")) {
        throw new Api400Error("bid status is still pending, please select accept or reject")
      } else if (bidInfo.status == "accept" && (status == "accept" || status == "reject")) {
        throw new Api400Error("bid has beed accepted, please select completed or canceled")
      } else if (bidInfo.status == "completed" || bidInfo.status == "canceled") {
        throw new Api400Error(`bid status already changed before`);
      }

      // change product status
      if (status == "completed") {
        await Products.update({ status: "sold" }, {
          where: { id: bidInfo.product_id }
        })
        .catch(error => {
          throw new Api400Error(error.message);
        })
      }

      // send notification
      if (status != "completed") {
        await Notification.create({
          user_id: bidInfo.buyer_id,
          product_id: bidInfo.product_id,
          as: "buyer",
          isRead: "N",
          message: (status == "accept") ? "Penawaran produk disetujui oleh penjual"
                 : (status == "reject") ? "Penawaran produk ditolak oleh penjual"
                 : (status == "canceled") ? "Transaksi dibatalkan oleh penjual"
                 : null
        })
      }

      // change bid status
      await Bids.update({ status }, {
        where: { id }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      return res.json({
        success: true,
        message: "bid status successfully changed"
      });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = TransactionController;
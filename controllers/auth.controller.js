require("dotenv").config();
const bcrypt     = require("bcrypt");
const jwt        = require("jsonwebtoken");

// API error
const { validationResult } = require("express-validator");
const { Api422Error, Api400Error } = require("../middlewares/errors/APIErrors");

// models 
const { Users, Profile, Token, sequelize } = require("../models");

class AuthController {
  static async register(req, res, next) {
    // init transaction
    const t = await sequelize.transaction();
    try {
      const errors = validationResult(req);
      // check validation error
      if (!errors.isEmpty()) {
        throw new Api422Error("validation error", errors.array());
      }

      // request body
      const { name, email, password } = req.body;
      // adding user
      await Users.create({
        email,
        password: bcrypt.hashSync(password, 10),
        name
      }, {
        transaction: t
      })
      .catch(error => {
        if (error.name == "SequelizeUniqueConstraintError") {
          throw new Api400Error("email already registered")
        } else {
          throw new Api400Error(error.message);
        }
      })

      // user success registered
      await t.commit();
      return res.status(201).json({
        success: true,
        message: "user successfully registered"
      });
    } catch (error) {
      await t.rollback()
      next(error);
    }
  }

  static async login(req, res, next) {
    try {
      const errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw new Api422Error("validation error", errors.array());
      }

      // request body
      const { email, password } = req.body;
      // check email
      const user = await Users.findOne({
        where: { email },
      });
      
      if (user) {
        // check password
        const passed = bcrypt.compareSync(password, user.password);
        if (passed) {
          // jwt require
          const {
            JWT_SECRET_KEY,
            JWT_TIME_EXPIRED,
            JWT_ISSUER
          } = process.env;
          // generate token
          const token = jwt.sign({
            id: user.id,
            email: user.email
          }, JWT_SECRET_KEY, {
            expiresIn: JWT_TIME_EXPIRED,
            issuer: JWT_ISSUER,
            audience: user.email
          });

          await Token
            .create({ user_id: user.id, token })
            .catch(error => {
              throw new Api400Error(error.message);
            })

          return res.json({
            success: true,
            message: "user successfully login",
            token: `Bearer ${token}`
          })
        } else {
          throw new Api400Error("email or password wrong!")
        }
      } else {
        throw new Api400Error("email or password wrong!")
      }
      
      return res.json(user);
    } catch (error) {
      next(error);
    }
  }

  static async logout(req, res, next) {
    try {
      // get data from middleware
      const { id } = req.user;

      await Token.destroy({
        where: {
          user_id: id
        }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      return res.json({
        success: true,
        message: "user successfully logout"
      });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = AuthController;
const cloudinary = require("cloudinary").v2;
const multer = require("multer");
const fs = require("fs");
const path = require("path");
const upload = require("../helper/multer.helper")().array("image");

// config cloudinary
cloudinary.config({
  cloud_name: process.env.CLD_CLOUD_NAME,
  api_key: process.env.CLD_API_KEY,
  api_secret: process.env.CLD_API_SECRET,
  secure: true
});

// API Error
const { Api404Error, Api400Error, Api422Error } = require("../middlewares/errors/APIErrors");

// Models
const { Products, productImages, Notification, Users, WishList } = require('../models');
const { Op } = require("sequelize");

class ProductController {
  static async createProduct(req, res, next) {
    upload(req, res, async function (err) {
      try {
        if (err instanceof multer.MulterError) {
          // A Multer error occurred when uploading.
          req.files.forEach(d => {
            fs.unlinkSync(path.join(__dirname, '../' + d.path));
          });
          throw new Api400Error(err.message);
        } else if (err) {
          // An unknown error occurred when uploading.
          req.files.forEach(d => {
            fs.unlinkSync(path.join(__dirname, '../' + d.path));
          });
          throw new Api400Error(err.message);
        }

        // generate uuid
        const id = require("crypto").randomUUID();
        // check image product
        if (req.files.length == 0) {
          req.files.forEach(d => {
            fs.unlinkSync(path.join(__dirname, '../' + d.path));
          });
          throw new Api400Error("image cannot be empty");
        } else if (req.files.length > 3) {
          req.files.forEach(d => {
            fs.unlinkSync(path.join(__dirname, '../' + d.path));
          });
          throw new Api400Error("maximum product images only 3 files")
        }

        // check profile seller
        const profile = await Users.findOne({
          where: { id: req.user.id },
          attributes: ["name", "city", "address", "phone_number", "avatar"]
        })
        .catch(error => {
          throw new Api400Error(error.message);
        });

        for (const d in profile) {
          if (profile[d] == null) {
            req.files.forEach(d => {
              fs.unlinkSync(path.join(__dirname, '../' + d.path));
            });
            throw new Api400Error("Your profile is not complete, please complete")
          }
        }

        // data body
        const { name, price, category, status, description } = req.body;
        const user_id = req.user.id;
        // adding data product
        const product = await Products.create({
          id, name, price, category, status, description, user_id
        })
          .catch(error => {
            if (error.name == "SequelizeValidationError") {
              throw new Api422Error("validation error", error.errors.map(d => {
                return {
                  value: d.value,
                  msg: d.message,
                  param: d.path,
                  location: "body"
                }
              }));
            }
            req.files.forEach(d => {
              fs.unlinkSync(path.join(__dirname, '../' + d.path));
            });
            throw new Api400Error(error.message);
          });

        // send notification
        if (status == "published") {
          await Notification.create({
            user_id: req.user.id,
            product_id: id,
            as: "seller",
            isRead: "N",
            message: "Product berhasil di terbitkan"
          })
          .catch(error => {
            throw new Api400Error(error.message);
          });
        }

        // upload image to cloudinary and insert to database
        req.files.forEach(async (d, i) => {
          await cloudinary.uploader.upload(d.path, { folder: "product" }, async (err, done) => {
            if (err) {
              throw new Api400Error(err.message);
            }
            // querying
            await productImages.create({
              product_id: id,
              path: done.secure_url
            })
              .catch(error => {
                throw new Api400Error(error);
              })
          });
          fs.unlinkSync(path.join(__dirname, '../' + d.path));
          if (i == req.files.length - 1)
            return res.status(201).json({
              success: true,
              message: "product successfully added",
              product
            });
        });
      } catch (error) {
        next(error);
      }
    })
  }

  static async listProduct(req, res, next) {
    try {
      const { page = 1, limit = 25, category = null, search = '' } = req.query;
      const categories = ['Hobi', 'Kendaraan', 'Baju', 'Elektronik', 'Kesehatan']

      // check authorization
      if (req.auth) {
        // notification
        const notification = await Notification.findAll({
          where: {
            user_id: req.user.id,
            isRead: "N"
          }
        })
          .catch(error => {
            throw new Api400Error(error.message);
          });

        // Get All Product
        const product = await Products.findAll({
          where: {
            // Request user id
            user_id: {
              [Op.not]: req.user.id
            },
            category: {
              [Op.in]: (category == null || category == '' || category == undefined) ? categories : [category]
            },
            name: {
              [Op.substring]: search
            },
            status: "published"
          },
          order: [["updatedAt", "DESC"]],
          limit,
          offset: +limit * +page - +limit,
          include: {
            model: productImages,
            as: "images",
            attributes: ["id", "path"],
          }
        })
          .catch(error => {
            throw new Api400Error(error.message);
          });

        return res.json({
          success: true,
          message: "data successfully retrieved",
          product,
          notification
        });
      } else {
        const product = await Products.findAll({
          where: {
            category: {
              [Op.in]: (category == null || category == '' || category == undefined) ? categories : [category]
            },
            name: {
              [Op.substring]: search
            },
            status: "published"
          },
          limit,
          offset: +limit * +page - +limit,
          order: [["updatedAt", "DESC"]],
          include: {
            model: productImages,
            as: "images",
            attributes: ["id", "path"],
          }
        })
          .catch(error => {
            throw new Api400Error(error.message);
          });

        return res.json({
          success: true,
          message: "data successfully retrieved",
          product
        });
      }

    } catch (error) {
      next(error)
    }
  }

  static async listProductSpesificUser(req, res, next) {
    try {
      // querying data product
      const product = await Products.findAll({
        where: { user_id: req.user.id },
        include: {
          model: productImages,
          as: 'images'
        }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      const notification = await Notification.findAll({
        where: {
          user_id: req.user.id,
          isRead: "N"
        }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });


      if (product) {
        return res.json({
          success: true,
          message: "data successfully retrieved",
          product,
          notification
        });
      } else {
        throw new Api404Error("product not found");
      }
    } catch (error) {
      next(error);
    }
  }

  static async previewProduct(req, res, next) {
    try {

      if (req.auth) {
        // querying data product
        const product = await Products.findOne({
          where: {
            id: req.params.id,
          },
          include: {
            model: productImages,
            as: 'images'
          }
        })
        .catch(error => {
          throw new Api400Error(error.message);
        });

        const notification = await Notification.findAll({
          where: {
            user_id: req.user.id,
            isRead: "N"
          }
        })
        .catch(error => {
          throw new Api400Error(error.message);
        });      
        
        if (product) {
          return res.json({
            success: true,
            message: "data successfully retrieved",
            product,
            notification
          });
        } else {
          throw new Api404Error("product not found");
        }
      } else {
        // querying data product
        const product = await Products.findOne({
          where: {
            id: req.params.id,
          },
          include: {
            model: productImages,
            as: 'images'
          }
        })
        .catch(error => {
          throw new Api400Error(error.message);
        });     
        
        if (product) {
          return res.json({
            success: true,
            message: "data successfully retrieved",
            product
          });
        } else {
          throw new Api404Error("product not found");
        }
      }
    } catch (error) {
      next(error);
    }
  }

  static async addImageProduct(req, res, next) {
    upload(req, res, async function(err) {
      try {
        if (err instanceof multer.MulterError) {
          // A Multer error occurred when uploading.
          req.files.forEach(d => {
            fs.unlinkSync(path.join(__dirname, '../' + d.path));
          });
          throw new Api400Error(err.message);
        } else if (err) {
          // An unknown error occurred when uploading.
          req.files.forEach(d => {
            fs.unlinkSync(path.join(__dirname, '../' + d.path));
          });
          throw new Api400Error(err.message);
        }
        // get data id product from params
        const { id } = req.params;
        // check total image
        const totalIMG = await productImages.count({
          where: {
            product_id: id
          }
        })
        .catch(error => {
          throw new Api400Error(error.message);
        });
  
        if ((totalIMG + req.files.length) > 3) {
          req.files.forEach(d => {
            fs.unlinkSync(path.join(__dirname, '../' + d.path));
          })
          throw new Api400Error("image has reached the maximum limit");
        }
  
        // insert image
        req.files.forEach(async (d, i) => {
          await cloudinary.uploader.upload(d.path, { folder: "product" }, async (err, done) => {
            if (err) {
              throw new Api400Error(err.message);
            }
            
            await productImages.create({
              product_id: id,
              path: done.secure_url
            })
            .catch(error => {
              throw new Api400Error(error.message);
            });

          });
          
          // remove all image after upload to cloud
          req.files.forEach(d => {
            fs.unlinkSync(path.join(__dirname, '../' + d.path));
          });

          if (i + 1 == req.files.length) {
            return res.status(201).json({
              success: true,
              message: "product image successfully added"
            });
          }
        });
      } catch (error) {
        next(error);
      }
    });
  }

  static async updateProduct(req, res, next) {
    try {
      // data body
      const { name, price, category, status, description } = req.body;
      const user_id = req.user.id;
      // adding data product
      const product = await Products.update({
        name, price, category, status, description, user_id
      }, {
        where: {
          id: req.params.product_id
        },
        returning: true,
      })
        .catch(error => {
          if (error.name == "SequelizeValidationError") {
            throw new Api422Error("validation error", error.errors.map(d => {
              return {
                value: d.value,
                msg: d.message,
                param: d.path,
                location: "body"
              }
            }));
          }
          throw new Api400Error(error.message);
        });

      return res.status(201).json({
        success: true,
        message: "product successfully updated",
        product: product[1][0]
      })
    } catch (error) {
      next(error);
    }
  }

  static async deleteImage(req, res, next) {
    try {
      // get data
      const { id } = req.params;
      const image = await productImages.findOne({
        where: {
          id
        }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      await productImages.destroy({
        where: {
          id
        }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      const url = image.path;
      let public_id = url.split('/').slice(7, 9);
      public_id[1] = public_id[1].split('.')[0];
      public_id = public_id.join('/');
      // destroy image
      await cloudinary.uploader.destroy(public_id);

      return res.json({
        success: false,
        message: "image product successfully deleted"
      });
    } catch (error) {
      next(error);
    }
  }

  static async deleteProduct(req, res, next) {
    try {
      // get data from middleware
      const { id } = req.params;
      const images = await productImages.findAll({
        where: {
          product_id: id
        }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      await Products.destroy({
        where: {
          id
        }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      images.forEach(async(d, i) => {
        const url = d.path;
        let public_id = url.split('/').slice(7, 9);
        public_id[1] = public_id[1].split('.')[0];
        public_id = public_id.join('/');
        // destroy avatar
        await cloudinary.uploader.destroy(public_id);        
      })

      return res.json({
        success: false,
        message: "product successfully deleted"
      });
    } catch (error) {
      next(error);
    }
  }

  static async releaseProduct(req, res, next) {
    try {
      // get id product
      const { id } = req.params;

      // query for change status
      await Products.update({ status: "published" }, {
        where: { id },
        returning: true
      })
        .catch(error => {
          throw new Api400Error(error.message);
        });

      // add notification
      await Notification.create({
        user_id: req.user.id,
        product_id: id,
        as: "seller",
        isRead: "N",
        message: "Product berhasil di terbitkan"
      })
        .catch(error => {
          throw new Api400Error(error.message);
        })

      return res.json({
        success: true,
        message: "product successfully released"
      })

    } catch (error) {
      next(error);
    }
  }

  static async getAllWishlist(req, res, next) {
    try {
      // querying
      const wishlist = await WishList.findAll({
        where: { user_id: req.user.id },
        attributes: ["id"],
        include: {
          model: Products,
          as: "products",
          include: {
            model: productImages,
            as: "images"
          }
        }
      })

      return res.json({
        success: true,
        message: "data wishlist successfully retrieved",
        wishlist
      });
    } catch (error) {
      next(error);
    }
  }

  static async addWishList(req, res, next) {
    try {
      // get data from params
      const { id } = req.params;
      // checking availability
      const checkAVB = await WishList.findOne({
        where: {
          user_id: req.user.id,
          product_id: id
        }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      if (checkAVB) {
        throw new Api400Error("product is already on the wishlist")
      }
      // add wishlist
      await WishList.create({
        user_id: req.user.id,
        product_id: id
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      return res.status(201).json({
        success: true,
        message: "product has been successfully added to the wishlist"
      });
    } catch (error) {
      next(error);
    }
  }

  static async removeWishList(req, res, next) {
    try {
      // get id wishlist from params
      const { id } = req.params;
      // check availability
      const checkAVB = await WishList.findOne({
        where: {
          user_id: req.user.id,
          product_id: id
        }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      if (checkAVB) {
        throw new Api400Error("product not found in wishlist")
      }

      // removing wishlist
      await WishList.destroy({
        where: { id, user_id: req.user.id }
      })
      .catch(error => {
        throw new Api400Error(error.message);
      });

      return res.json({
        success: true,
        message: "wishlist successfully removed"
      });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = ProductController;
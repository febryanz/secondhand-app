'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Products', [
      {
        id: "ec0d38b6-a639-41d5-a4f7-5c0f9573eb8a",
        user_id: "3afac560-f480-4447-a8df-c6e0c36b9ed6",
        name: "product1",
        price: 10000,
        category: "Kendaraan",
        status: "published",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "a480bd94-a9f2-41c4-bfe4-ff80e9b9b110",
        user_id: "e99154f6-1a5f-422c-9a6a-336ca55f7861",
        name: "product2",
        price: 20000,
        category: "Baju",
        status: "published",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "aa8d384f-6e12-4d92-836f-16182af2b3a0",
        user_id: "89ce5c8e-aace-480c-9b53-b2bce625a016",
        name: "product3",
        price: 30000,
        category: "Elektronik",
        status: "published",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "b5652328-058c-4575-972b-11740940cffa",
        user_id: "89ce5c8e-aace-480c-9b53-b2bce625a016",
        name: "product4",
        price: 30000,
        category: "Elektronik",
        status: "published",
        description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Products', null, {});
  }
};

'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert("productImages", [
      {
        id: "0b1b6868-5984-42fc-94e0-8ca004caceee",
        product_id: "ec0d38b6-a639-41d5-a4f7-5c0f9573eb8a",
        path: "https://res.cloudinary.com/didik27/image/upload/v1656039636/product/sefqpxropan2jbpfek1c.png",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "7dcc5172-a0b5-4291-b16c-01f4c0f3d0b9",
        product_id: "a480bd94-a9f2-41c4-bfe4-ff80e9b9b110",
        path: "https://res.cloudinary.com/didik27/image/upload/v1656039636/product/sefqpxropan2jbpfek1c.png",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "57f9698f-3302-438a-b972-5a19f0cc9bed",
        product_id: "aa8d384f-6e12-4d92-836f-16182af2b3a0",
        path: "https://res.cloudinary.com/didik27/image/upload/v1656039636/product/sefqpxropan2jbpfek1c.png",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "b2d70c21-7cb7-4b4f-9c08-dde8c18f4e46",
        product_id: "b5652328-058c-4575-972b-11740940cffa",
        path: "https://res.cloudinary.com/didik27/image/upload/v1656039636/product/sefqpxropan2jbpfek1c.png",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ])
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('productImages', null, {});
  }
};

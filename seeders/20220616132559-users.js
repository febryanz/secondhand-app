'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Users', [
      {
        id: "3afac560-f480-4447-a8df-c6e0c36b9ed6",
        email: "user1@binar.com",
        password: require("bcrypt").hashSync("user1234", 10),
        name: "user1",
        city: "Probolinggo",
        phone_number: "08123456789",
        address: "East Java, Indonesia",
        avatar: "https://res.cloudinary.com/didik27/image/upload/v1656039605/profile/abtcnwdiebfotccmq6wh.png",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "e99154f6-1a5f-422c-9a6a-336ca55f7861",
        email: "user2@binar.com",
        password: require("bcrypt").hashSync("user1234", 10),
        name: "user2",
        city: "Padang",
        phone_number: "08123456789",
        address: "West Sumatra, Indonesia",
        avatar: "https://res.cloudinary.com/didik27/image/upload/v1656039605/profile/abtcnwdiebfotccmq6wh.png",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: "89ce5c8e-aace-480c-9b53-b2bce625a016",
        email: "user3@binar.com",
        password: require("bcrypt").hashSync("user1234", 10),
        name: "user3",
        city: "Bandung",
        phone_number: "08123456789",
        address: "West Java, Indonesia",
        avatar: "https://res.cloudinary.com/didik27/image/upload/v1656039605/profile/abtcnwdiebfotccmq6wh.png",
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Users', null, {});
  }
};

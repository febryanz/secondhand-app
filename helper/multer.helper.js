const multer = require("multer");
const fs = require('fs')

module.exports = () => {
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      if(!fs.existsSync('public/images')){
        fs.mkdirSync('public')
        fs.mkdirSync('public/images')
      }
      cb(null, "public/images")
    },
    filename: function (req, file, cb) {
      cb(null, `${Date.now()}.${file.mimetype.split('/').pop()}`)
    },
  })
  
  const upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
      if (file.mimetype == "image/png" || file.mimetype == "image/jpeg" || file.mimetype == "image/jpg") {
        cb(null, true);
      } else {
        cb("file type not supported", false);
      }

    }
  });

  return upload;
}


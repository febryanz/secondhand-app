'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Bids', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      product_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      buyer_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      seller_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      price_bid: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      status: {
        type: Sequelize.ENUM,
        values: ["accept", "reject", "canceled", "completed", "pending"]
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });

    await queryInterface.addConstraint("Bids", {
      fields: ["product_id"],
      type: "foreign key",
      references: {
        table: "Products",
        field: "id"
      },
      onDelete: "CASCADE"
    });

    await queryInterface.addConstraint("Bids", {
      fields: ["buyer_id"],
      type: "foreign key",
      references: {
        table: "Users",
        field: "id"
      },
      onDelete: "CASCADE"
    });

    await queryInterface.addConstraint("Bids", {
      fields: ["seller_id"],
      type: "foreign key",
      references: {
        table: "Users",
        field: "id"
      },
      onDelete: "CASCADE"
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Bids');
  }
};
"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Products", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      user_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false
      },
      price: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      category: {
        type: Sequelize.ENUM,
        allowNull: false,
        values: ["Hobi", "Kendaraan", "Baju", "Elektronik", "Kesehatan"]
      },
      description: {
        type: Sequelize.TEXT,
        allowNull: false
      },
      status: {
        type: Sequelize.ENUM,
        allowNull: false,
        values: ["sold", "published", "unpublished"]
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });

    await queryInterface.addConstraint("Products", {
      fields: ["user_id"],
      type: "foreign key",
      references: {
        table: "Users",
        field: "id"
      },
      onDelete: "CASCADE"
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Products");
  }
};
'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('productImages', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.UUID
      },
      product_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      path: {
        type: Sequelize.STRING,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });

    await queryInterface.addConstraint("productImages", {
      fields: ["product_id"],
      type: "foreign key",
      references: {
        table: "Products",
        field: "id"
      },
      onDelete: "CASCADE"
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('productImages');
  }
};
'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('Notifications', {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.INTEGER,
        autoIncrement: true
      },
      user_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      product_id: {
        type: Sequelize.UUID,
        allowNull: false
      },
      as: {
        type: Sequelize.ENUM,
        values: ['seller', 'buyer'],
        allowNull: false
      },
      isRead: {
        type: Sequelize.ENUM,
        values: ['Y', 'N'],
        allowNull: false
      },
      message: {
        type: Sequelize.STRING,
        allowNull: false
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });

    await queryInterface.addConstraint("Notifications", {
      fields: ["product_id"],
      type: "foreign key",
      references: {
        table: "Products",
        field: "id"
      },
      onDelete: "CASCADE"
    });

    await queryInterface.addConstraint("Notifications", {
      fields: ["user_id"],
      type: "foreign key",
      references: {
        table: "Users",
        field: "id"
      },
      onDelete: "CASCADE"
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('Notifications');
  }
};
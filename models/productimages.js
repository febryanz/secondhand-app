'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class productImages extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      productImages.belongsTo(models.Products, { foreignKey: 'product_id', as: 'product' });
    }
  }
  productImages.init({
    product_id: {
      type: DataTypes.UUID,
      allowNull: false
    },
    path: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    hooks: {
      beforeCreate: (image, otp) => {
        image.id = require("crypto").randomUUID()
      }
    },
    sequelize,
    modelName: 'productImages',
  });
  return productImages;
};
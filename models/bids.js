'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Bids extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Bids.belongsTo(models.Products, { foreignKey: "product_id", as: "product" });
      Bids.belongsTo(models.Users, { foreignKey: "buyer_id", as: "buyer" });
      Bids.belongsTo(models.Users, { foreignKey: "seller_id", as: "seller" });
    }
  }
  Bids.init({
    product_id: {
      type: DataTypes.UUID,
      allowNull: false
    },
    buyer_id: {
      type: DataTypes.UUID,
      allowNull: false
    },
    price_bid: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    status: {
      type: DataTypes.ENUM,
      values: ["accept", "reject", "canceled", "completed", "pending"],
      defaultValue: 'pending'
    }
  }, {
    hooks: {
      beforeCreate: (bid, otp) => {
        bid.id = require("crypto").randomUUID()
      }
    },
    sequelize,
    modelName: 'Bids',
  });
  return Bids;
};
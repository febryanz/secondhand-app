'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Products extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Products.hasOne(models.Notification, { foreignKey: 'product_id', as: 'notification' });
      Products.hasMany(models.productImages, { foreignKey: 'product_id', as: 'images' });
      Products.hasMany(models.Bids, { foreignKey: "product_id", as: "bids" });
    }
  }
  Products.init({
    user_id: {
      type: DataTypes.UUID,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: { msg: "name cannot be empty" }
      }
    },
    price: {
      type: DataTypes.INTEGER,
      isNumeric: true,
      validate: {
        notEmpty: { msg: "price cannot be null" },
        isNumeric: { msg: "price must be number" }
      }
    },
    category: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: { msg: "category cannot be null" },
        isIn: {
          msg: "category must contain Hobi, Kendaraan, Baju, Elektronik or Kesehatan",
          args: [["Hobi", "Kendaraan", "Baju", "Elektronik", "Kesehatan"]]
        }
      }
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notEmpty: { msg: "name cannot be empty" }
      }
    },
    status: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: { msg: "status cannot be null" },
        isIn: {
          msg: "isRealese must contain sold, unpublished or published",
          args: [["sold", "published", "unpublished"]]
        }
      }
    }
  }, {
    hooks: {
      beforeCreate: (product, opt) => {
        product.id = require("crypto").randomUUID()
      }
    },
    sequelize,
    modelName: 'Products',
  });
  return Products;
};
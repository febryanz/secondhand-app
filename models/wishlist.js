'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class WishList extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      WishList.belongsTo(models.Users, { foreignKey: "user_id", as: "user" });
      WishList.belongsTo(models.Products, { foreignKey: "product_id", as: "products" });
    }
  }
  WishList.init({
    user_id: {
      type: DataTypes.UUID,
      allowNull: false
    },
    product_id: {
      type: DataTypes.UUID,
      allowNull: false
    }
  }, {
    hooks: {
      beforeCreate: (wish, opt) => {
        wish.id = require("crypto").randomUUID();
      }
    },
    sequelize,
    modelName: 'WishList',
  });
  return WishList;
};
'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Notification extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Notification.belongsTo(models.Users, { foreignKey: 'user_id', as: 'user' });
    }
  }
  Notification.init({
    user_id: {
      type: DataTypes.UUID,
      allowNull: false
    },
    product_id: {
      type: DataTypes.UUID,
      allowNull: false
    },
    as: {
      type: DataTypes.ENUM,
      values: ['seller', 'buyer'],
      allowNull: false
    },
    isRead: {
      type: DataTypes.ENUM,
      values: ['Y', 'N'],
      allowNull: false
    },
    message: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    sequelize,
    modelName: 'Notification',
  });
  return Notification;
};
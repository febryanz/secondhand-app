'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Users extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Users.hasMany(models.Products, { foreignKey: 'user_id', as: 'products' });
      Users.hasMany(models.Notification, { foreignKey: 'user_id', as: 'notifications' });
      Users.hasMany(models.Token, { foreignKey: 'user_id', as: 'tokens' });
      Users.hasMany(models.Bids, { foreignKey: "buyer_id", as: "bid" });
      Users.hasMany(models.Bids, { foreignKey: "seller_id", as: "bids" });
    }
  }
  Users.init({
    email: {
      type: DataTypes.STRING,
      allowNull: false,
      unique: true
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false
    },
    name: {
      type: DataTypes.STRING
    },
    city: {
      type: DataTypes.STRING
    },
    address: {
      type: DataTypes.STRING
    },
    phone_number: {
      type: DataTypes.STRING
    },
    avatar: {
      type: DataTypes.STRING
    }
  }, {
    hooks: {
      beforeCreate: function (user, opt) {
        user.id = require('crypto').randomUUID()
      }
    },
    sequelize,
    modelName: 'Users',
  });
  return Users;
};
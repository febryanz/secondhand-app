const router = require("express").Router();

// controller
const controller = require("../controllers/product.controller");
// middleware
const authorize = require("../middlewares/authorize");
const checkAuth = require("../middlewares/checkAuthorization")

// route GET
router.get("/", checkAuth, controller.listProduct);
router.get("/wishlist", authorize, controller.getAllWishlist);
router.get("/seller", authorize, controller.listProductSpesificUser);
router.get("/:id", checkAuth, controller.previewProduct);

// route PUT
router.put("/:product_id", authorize, controller.updateProduct);

// route PATCH
router.patch("/:id/publish", authorize, controller.releaseProduct);

// route POST
router.post("/", authorize, controller.createProduct);
router.post("/:id/image", authorize, controller.addImageProduct);
router.post("/:id/wishlist", authorize, controller.addWishList);

// route DELETE
router.delete("/:id", authorize, controller.deleteProduct);
router.delete("/:id/wishlist", authorize, controller.removeWishList);
router.delete("/:id/image", authorize, controller.deleteImage)

module.exports = router;
const router = require("express").Router();

// controller
const controller = require("../controllers/transaction.controller");
// middleware
const authorize = require("../middlewares/authorize");
// validation
const bidSchema       = require("../validations/bid");
const bidStatusSchema = require("../validations/bidStatus"); 

// GET route
router.get("/history", authorize, controller.getTransactionHistories);
router.get("/bids", authorize, controller.getEntryBid);
router.get("/:id/bid", authorize, controller.getDetailBid);

// POST route
router.post("/:id/bid", authorize, bidSchema, controller.BidsProduct);

// PATCH route
router.patch("/:id/bid/:status", authorize, bidStatusSchema, controller.changeStatusBid);

module.exports = router;
const router = require("express").Router();

// controller
const controller = require("../controllers/auth.controller");

// middleware
const authorize = require("../middlewares/authorize");

// validation
const registerSchema = require("../validations/register");
const loginSchema    = require("../validations/login");

// Route POST
router.post("/register", registerSchema, controller.register);
router.post("/login", loginSchema, controller.login);

// Route DELETE
router.delete("/logout", authorize, controller.logout);

module.exports = router;

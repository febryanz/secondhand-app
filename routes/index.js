const router = require("express").Router();

// list router
const auth         = require("./auth.router");
const profile      = require("./profile.router"); 
const product      = require("./product.router");
const transaction  = require("./transaction.router");
const notification = require("./notification.router"); 

router.use("/v1/auth", auth);
router.use("/v1/profile", profile);
router.use("/v1/product", product);
router.use("/v1/transaction", transaction);
router.use("/v1/notification", notification);

module.exports = router;
const router = require("express").Router();

// controller
const controller = require("../controllers/notification.controller");
// middleware
const authorize = require("../middlewares/authorize");

// route get
router.get("/", authorize, controller.getAllNotification);

// route PATCH
router.patch("/:id/status", authorize, controller.changeNotificationStatus);

module.exports = router;
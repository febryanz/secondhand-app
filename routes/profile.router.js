const router = require("express").Router();

// controller
const controller = require("../controllers/profile.controller");
// middleware
const authorize = require("../middlewares/authorize");
// validation
const profileSchema = require("../validations/profile");

// route GET
router.get("/", authorize, controller.getDetails);

// route PUT
router.put("/", authorize, profileSchema, controller.updateProfile);

module.exports = router;